/*
 * Autor: Jan Herec
 * Datum vytvoreni: 16.2.2018
 * Kodovani: UTF-8
 * Popis: Projekt do predmetu WAP na FIT VUT, varianta: Dynamicka tabulka
 * Zadani projektu: Mejme libovolnou HTML tabulku umistenou v dokumentu opatrenou identifikatorem id. Vytvorte funkci v JavaScriptu s parametrem id tabulky (pripadne dalsimi parametry), ktera nad prvni radek tabulky prida vstupni pole umoznujici filtrovani obsahu a ikony pro razeni obsahu sloupce vzestupne a sestupne. Filtr funguje tak, ze radek tabulky se zobrazi, pokud bunka v prislusnem sloupci obsahuje podretezec zadany do vstupniho pole nad timto sloupcem. Tvurce stranky muze pro kazdy sloupec zvolit, zda se prvky daneho sloupce radi jako cisla nebo jako retezce. Pripadne prvky thead a tfoot v tabulce jsou pri filtrovani a razeni ignorovany - zpracovava se pouze jeden prvek tbody. Predpokladejte, ze vsechny hodnoty colspan a rowspan jsou rovny 1. Pro filtrovani se uvazuje textovy obsah bunek tabulky, jeho pripadne strukturovani na elementy se ignoruje.
 */

/**
 * Trida obsahuje metody pro zajisteni dynamicke tabulky,
 * kdy mame nejakou HTML tabulku, jejiz zaznamy/radky budeme moci filtrovat a radit podle jednotlivych sloupcu
 */
class DynamicTable {

    /**
     * Konstruktor inicializuje atributy objektu
     */
    constructor ( ) {
        this.columnSortOrder = null                 // poradi sloupce podle ktereho se radi
        this.columnSortType = null                  // typ razeni (up nebo down, resp. ASC nebo DESC)
        this.tableId = null                         // id tabulky
        this.columnSortByStringOrNumber = null      // objekt, jehoz vlastnosti col1, col2, ...coln urcuji jak se radi sloupce - jestli jako retezce (str) nebo cisla (num)
        this.table = null                           // aktualni zobrazena tabulka
        this.originalTableBody = null               // puvodni tabulka - respektive jeji cast tbody (ktera u aktualni tabulky muze byt jina)
        this.lastClickedSortArrow = null            // posledne kliknuta sipka pro razeni
    }

    /**
     * Metoda, ktera tvori rozhrani pro vytvoreni dynamicke tabulky (vytvori ovladaci prvky pro razeni/filtrovani a jejich obsluzne udalosti)
     * @param id id tabulky
     * @param columnSortByStringOrNumber objekt s vlastnostmi col1, col2, ...coln, které urcuji jak se radi sloupce - jestli jako retezce (str) nebo cisla (num)
     *        Pokud nektera vlastnost/sloupec chybi, uvazuje se str.
     */
    dynamicTable(id, columnSortByStringOrNumber) {

        // aby se nam nepletlo this (aktualni element pri obsluze udalosti) s this (objekt DynamicTable), tak zavedeme self reprezentujici objekt DynamicTable
        var self = this

        self.tableId = id
        self.table = document.getElementById(id)
        // ulozime si hlubokou kopii originalni tabulky (naklonujeme ji)
        self.originalTableBody = self.table.tBodies[0].cloneNode(true)

        // pokud uzivatel nespecifikoval parametr columnSortByStringOrNumber, tak implicitne nastavime ze se vsechny sloupce radi jako retezce (str)
        if (columnSortByStringOrNumber == null) {
            columnSortByStringOrNumber = new Object()
            for (var i = 1; i <= self.originalTableBody.rows[0].cells.length; i++) {
                columnSortByStringOrNumber["col" + i] = "str"
            }
        }
        self.columnSortByStringOrNumber = columnSortByStringOrNumber

        // vytvorime ovladaci prvky (pro razeni/filtrovani)
        self.createControlElements(self)
        // vytvorime obsluzne udalosti ovladacich prvku pro razeni/filtrovani
        self.createSortEventHandlers(self)
        self.createFilterEventHandlers(self)

        // resetujeme textove inputy pro filtrovani, protoze v Microsoft Edge se atomaticky doplnuji i po refreshi stranky
        setTimeout(function() {
            var controlsFilters = document.getElementsByClassName("dynamic-table-controls-filter" + self.tableId)
            for (var i = 0; i < controlsFilters.length; i++) {
                controlsFilters[i].value = ''
            }
        }, 1000)
    }

    /**
     * Metoda pro vytvoreni ovladacich prvku pro razeni a filtrovani
     * @param self alias pro this, tedy aktualni instance DynamicTable
     */
    createControlElements(self) {
        // nejdrive pokud neexistuje hlavicka, tak ji vytvorime
        if (self.table.tHead == null) {
            var tableHead = document.createElement("thead")
            tableHead.insertRow()
            for (var i = 0; i < self.originalTableBody.rows[0].cells.length; i++) {
                tableHead.rows[0].innerHTML += "<th></th>"
            }
            self.table.appendChild(tableHead)
        }

        // pridame ovladaci prvky do bunek hlavičky
        for (var i = 0; i < self.originalTableBody.rows[0].cells.length; i++) {
            var widthOfInput = self.table.tBodies[0].rows[0].cells[i].offsetWidth // spocitame sirku textoveho inputu na zaklade puvodne zobrazovane sirky daneho sloupce, tímto vlastne adaptivne/proporcne nastavime sirku textoveho inputu
            var tableHeadCell = self.table.tHead.rows[0].cells[i]
            tableHeadCell.innerHTML = '<input autocomplete="off" type="text" style="width: '+ widthOfInput +'px" class="dynamic-table-controls-filter dynamic-table-controls-filter' + self.tableId + '" data-column-order="' + (i + 1) + '"/><div id="dynamic-table-controls-sort-div"><a href="#" class="dynamic-table-controls-sort dynamic-table-controls-sort' + self.tableId + '" data-type="up" data-column-order="' + (i + 1) + '">&#9650</a><a href="#" class="dynamic-table-controls-sort dynamic-table-controls-sort' + self.tableId + '" data-type="down" data-column-order="' + (i + 1) + '">&#9660;</a></div>' + tableHeadCell.innerHTML
        }
    }

    /**
     * Metoda pro vytvoreni obsluznych udalosti ovladacich prvku pro razeni
     * @param self alias pro this, tedy aktualni instance DynamicTable
     */
    createSortEventHandlers(self) {
        // ziskame vsechny ovladaci prvky pro razeni (radici sipky)
        var controlsSort = document.getElementsByClassName("dynamic-table-controls-sort" + self.tableId)

        // vsem ovladacim prvkum pro razeni (radicim sipkam) nastavime obsluhu udalosti, kdy uzivatel klikne na radici sipku
        for (var i = 0; i < controlsSort.length; i++) {
            controlsSort[i].addEventListener("click", function (event) {
                event.preventDefault()

                // pokud existuje nejake nastavene razeni, tedy jsme nekdy v minulosti klikli na nejakou sipku pro razeni, tak ji odebereme tridu, ktera tuto sipku zvyraznuje
                if (self.lastClickedSortArrow) {
                    self.lastClickedSortArrow.classList.remove("dynamic-table-last-clicked-sort-arrow")
                }
                // zvyrazime aktualni sipku pro razeni na kterou jsme klikli
                this.classList.add("dynamic-table-last-clicked-sort-arrow")
                self.lastClickedSortArrow = this // ulozime si sipku pro razeni na kterou jsme klikli

                // ziskame radky aktualni tabulky
                var tableBodyRows = self.getTableBodyRows(self)
                // zjistime sloupec podle ktereho radime a typ razeni
                self.columnSortType = this.getAttribute("data-type")
                self.columnSortOrder = parseInt(this.getAttribute("data-column-order"))
                // seradime radky podle daneho sloupce a daneho typu razeni
                self.sortRows(self, tableBodyRows)

                // serazene radky nahradi aktualni radky v tbody tabulky
                self.table.tBodies[0].innerHTML = ""
                for (var j = 0; j < tableBodyRows.length; j++) {
                    self.table.tBodies[0].appendChild(tableBodyRows[j])
                }
            });
        }
    }

    /**
     * Metoda pro vytvoreni obsluznych udalosti ovladacich prvku pro filtrovani
     * @param self alias pro this, tedy aktualni instance DynamicTable
     */
    createFilterEventHandlers(self) {
        // ziskame vsechny ovladaci prvky pro filtorvani (vstupni pole)
        var controlsFilters = document.getElementsByClassName("dynamic-table-controls-filter" + self.tableId)

        // vsem ovladacim prvkum pro filtrovani (vstupnim polim) nastavime obsluhu udalosti, kdy uzivatel zmeni hodnotu v tomto poli
        for (var i = 0; i < controlsFilters.length; i++) {
            controlsFilters[i].addEventListener("input", function () {
                // ziskame pole filtru pro vsechny sloupce
                var columnFilters = new Array()
                for (var j = 0; j < controlsFilters.length; j++) {
                    columnFilters[j] = controlsFilters[j].value
                }

                // nalezneme relevantni radky kde hodnoty sloupcu odpovidaji nastavenym filtrum (tedy sloupce obsahuji podretezce definovane ve filtrech)
                var p = 0
                var relevantRows = new Array()
                for (var j = 0; j < self.originalTableBody.rows.length; j++) {

                    var rowSuitsFilters = true
                    // postupne pro vsechny sloupce/pole daneho radku overujeme jestli vyhovuji sloupcovym filtrum
                    for (var k = 0; k < self.originalTableBody.rows[0].cells.length; k++) {
                        var substringFound = (self.originalTableBody.rows[j].cells[k].textContent).search(new RegExp(columnFilters[k], "i")) !== -1;
                        rowSuitsFilters = rowSuitsFilters && substringFound
                    }

                    // radek vyhovuje filtrum, pridaneme jej  mez relevantni radky
                    if (rowSuitsFilters) {
                        relevantRows[p++] = (self.originalTableBody.rows[j]).cloneNode(true)
                    }
                }

                // pokud je nstavene razeni, tak relevantni radky navic jeste seradime
                if (self.columnSortOrder != null) {
                    self.sortRows(self, relevantRows)
                }

                // vyfiltrovane (relevantni) a pripadne serazene radky nahradi aktualni radky v tbody tabulky
                self.table.tBodies[0].innerHTML = ""
                for (var j = 0; j < relevantRows.length; j++) {
                    self.table.tBodies[0].appendChild(relevantRows[j])
                }
            });
        }
    }

    /**
     * Funkce vrati naklonovane radky aktualni tabulky
     * @param self alias pro this, tedy aktualni instance DynamicTable
     */

    getTableBodyRows(self) {
        var tableBodyRows = new Array()
        for (var j = 0; j < self.table.tBodies[0].rows.length; j++) {
            tableBodyRows[j] = (self.table.tBodies[0].rows[j]).cloneNode(true)
        }
        return tableBodyRows
    }

    /**
     * Funkce seradi radky tabulky vzestupne nebo sestupne podle zvoleneho sloupce
     * @param self alias pro this, tedy aktualni instance DynamicTable
     * @param rows radky tabulky, ktere se budou radit
     */
    sortRows(self, rows) {

        // provedeme prevod typu razeni z retezce na cislo aby se lepe delali aritmeticke vypocty
        if (self.columnSortType == "up") {
            var sortType = 1
        }
        else if (self.columnSortType == "down") {
            var sortType = -1
        }

        // prevedeme poradi sloupce na jeho index
        var columnIndex = self.columnSortOrder - 1
        // zjistime jestli se prvky daneho sloupce radi jako cisla nebo retezce
        var sortByStringOrNumber = self.columnSortByStringOrNumber["col" + self.columnSortOrder]

        // seradime radky a definujeme vlastni funkci pro porovnavani hodnot pri razeni (potrebujeme porovnavat radky dle jejich urciteho sloupce)
        rows.sort(function (a, b) {
            // pokud radime jako retezce (v pripade null implicitne radime jako retezce)
            if (sortByStringOrNumber == "str" || sortByStringOrNumber == null) {
                return (a.cells[columnIndex].textContent).localeCompare(b.cells[columnIndex].textContent) * sortType
            }
            // pokud radime jako cisla
            else if (sortByStringOrNumber == "num") {
                return (parseFloat(a.cells[columnIndex].textContent) - parseFloat(b.cells[columnIndex].textContent)) * sortType
            }
        })
    }
}
